@extends('layouts.app')

@section('content')
<div class="row" style="margin-bottom: 20px;">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <h3>show Blogs</h3>
        </div>

    </div>
</div>


<table class="table table-success">
    <tr>
        <th>id</th>
        <th>title</th>
        <th>summary</th>


        <th></th>
    </tr>
    @foreach ($posts as $post)

            <td>{{ $post->id }}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->read_more }}</td>


          <td>
              <a class="btn btn-light" href="{{route('posts.show' , $post->id)}}" >read more</a>
           </td>
        </tr>

@endforeach
@endsection
