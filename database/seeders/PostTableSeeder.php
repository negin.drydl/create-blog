<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker= \Faker\Factory::create();
        foreach (range(1,10) as $item) {
            Post::create ([
                "title"=> $faker->text(20),
                "read_more"=> $faker->text(40),
                "content"=> $faker->text(70),
            ]);
    }

    }
}
